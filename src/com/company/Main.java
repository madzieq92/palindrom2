package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Wpisz słowo: ");
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        int result;

        char[] wordArray = word.toCharArray();
        System.out.println("Słowo: " + word);
        result = checkWordIsPalindrom(wordArray, word);

        if (result == 1) {
            System.out.println("Twoje słowo jest palindromem.");
        } else {
            System.out.println("Twoje słowo nie jest palindromem.");
        }
    }

    public static int checkWordIsPalindrom(char[] wordArray, String word) {
        int i = 0;
        int j = word.length() - 1;
        int status = 0;

        while (wordArray[i] == wordArray[j]) {
            i = i + 1;
            j = j - 1;
            if (i == word.length() - 1) {
                status = 1;
                break;
            }
        }
        return status;
    }
}

